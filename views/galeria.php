<main class="mt-5 pt-5">
    <div class="container">
        <section class="mb-4">
            <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-1z" data-slide-to="1"></li>
                    <li data-target="#carousel-example-1z" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="<?= BASEURL ?>assets/img/pavao.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?= BASEURL ?>assets/img/sl.jpg" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?= BASEURL ?>assets/img/gato.jpg" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>
        <section class="text-center">
            <div class="row">
                <div class="col-lg-4 col-md-12 mb-3">
                    <div class="view overlay z-depth-1-half">
                        <img src="<?= BASEURL ?>assets/img/rs.jpg" class="img-fluid" alt="">
                        <a>
                            <div class="mask rgba-white-light"></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-3">
                    <div class="view overlay z-depth-1-half">
                        <img src="<?= BASEURL ?>assets/img/violino.jpg" class="img-fluid" alt="">
                        <a>
                            <div class="mask rgba-white-light"></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-3">
                    <div class="view overlay z-depth-1-half">
                        <img src="<?= BASEURL ?>assets/img/hp.jpeg" class="img-fluid" alt="">
                        <a>
                            <div class="mask rgba-white-light"></div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <div class="view overlay z-depth-1-half">
                        <img src="<?= BASEURL ?>assets/img/praia.jpg" class="img-fluid" alt="">
                        <a>
                            <div class="mask rgba-white-light"></div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="view overlay z-depth-1-half">
                        <img src="<?= BASEURL ?>assets/img/ceu.jpg" class="img-fluid" alt="">
                        <a>
                            <div class="mask rgba-white-light"></div>
                        </a>
                   </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-3">
                    <div class="view overlay z-depth-1-half">
                        <img src="<?= BASEURL ?>assets/img/pedra.jpg" class="img-fluid" alt="">
                        <a>
                            <div class="mask rgba-white-light"></div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>