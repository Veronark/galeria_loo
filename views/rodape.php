<footer class="page-footer text-center font-small mt-4 wow fadeIn">
  <div class="container">
    <div class="row">
      <div class="col-md-12 py-5">
        <div class="mb-5 flex-center">
          <a target="_blank" href="https://www.facebook.com/shantal.m.mantovani" class="fb-ic">
            <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <a target="_blank" href="https://twitter.com/?lang=pt" class="tw-ic">
            <i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <a target="_blank" href="https://www.instagram.com/montonavi_/" class="ins-ic">
            <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <a target="_blank" href="https://br.pinterest.com/shantalmorais/" class="pin-ic">
            <i class="fab fa-pinterest fa-lg white-text fa-2x"> </i>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-copyright py-3">
      © 2019 Copyright:
      <a href="http://hospedagem.ifspguarulhos.edu.br/~gu1800078/a/" target="_blank"> Shantal de Morais Mantovani </a>
  </div>
</footer>