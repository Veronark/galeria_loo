<section>
	<div class="container-fluid">
		<div class="row mt-5 pt-5">
			<div class="col-md-12 text-center">
				<h1>Galeria de Imagens</h1>
			</div>
		</div>
		<hr class="mb-5">
		<div class="row mx-auto justify-content-center pt-3">
            <?php echo $galeria ?>
        </div>
	</div>
</section>