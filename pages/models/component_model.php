<?php

include_once APPPATH.'libraries/util/DB.php';
include APPPATH.'component/libraries/component/Card.php';

function get_cards(){
    $db = new DB();
    $v = $db->get('upload');
    $html = '';

    foreach($v AS $data){
        $title = $data['nome']; 
        $content = $data['descricao'];
        $category = $data['categoria'];
        $archive = $data['arquivo'];
        $id = $data['id'];
		
        $card = new Card($title, $category, $content, $archive, $id);
        $html .= $card->getHTML();   
    }
    return $html;
}