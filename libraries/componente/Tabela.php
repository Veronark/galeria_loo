<?php

class Tabela{
    private $dados;
    private $rotulos;

    function __construct($dados, $rotulos = null){
        $this->rotulos = $rotulos;
        $this->dados = $dados;
    }

    public function getHTML(){
        $html = '<table class="table"><tr>';
        foreach($this->rotulos as $rotulo){
            $html .= '<th>'.$rotulo.'</th>';
        }
        $html .= '</tr>';
   
        foreach($this->dados as $row){
            $html .= '<tr>';
            foreach($row as $coluna){
                $html .= "<td>$coluna</td>";
            }
            $html .= $this->actionButtons($row['id']);
        }   
        return $html .= '</tr></table>';
    }

    private function actionButtons($id){
        $html = '<td><a href="'.BASEURL.'upload/editar.php?id='.$id.'">';
        $html .= '<i class="far fa-edit mr-3 green-text"></i></a>';
        $html .= '<td><a href="'.BASEURL.'upload/deletar.php?id='.$id.'">';
        $html .= '<i class="fas fa-trash red-text"></i></a></td>';
        return $html;
    }

}

?>